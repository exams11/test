from xml.dom.minidom import CharacterData
from peewee import SqliteDatabase, AutoField, CharField, DateField, ForeignKeyField, Model

db = SqliteDatabase('califications.db')

class UserModel(Model):
   user_id = AutoField()
   dni = CharField(unique=True)
   nombre = CharField()
   apellido = CharField()
   password = CharField()
   email = CharField(unique=True)
   class Meta:
       database = db

class CalificationModel(Model):
   calification_id = AutoField()
   course = CharField()
   credits = DateField()
   calification = DateField()
   notes = CharField()
   user_id = ForeignKeyField(UserModel)
   class Meta:
       database = db

db.connect()
db.create_tables([UserModel, CalificationModel])

