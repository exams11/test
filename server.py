# Run with "python server.py"

from array import array
from bottle import run, route, request, response
from schemas import UserSchema, CalificationSchema
from database import UserModel, CalificationModel
import json

@route('/user', method='POST')
def create_user():
    response.content_type = 'application/json'
    user = UserSchema()
    body = json.loads(request.body.read())
    res = user.load(body)
    if res.errors:
        return {
            "success" : False,
            "errors" : res.errors
        }
    else:
        new_user = UserModel(
            dni = body["dni"],
            nombre = body["nombre"],
            apellido = body["apellido"],
            password = body["password"],
            email = body["email"]
        )
        try:
            new_user.save()
            return {
                "success" : True,
                "data" : res.data
            }
        except  Exception as e:
            return {
            "success" : False,
            "errors" : "El registro existe en BD"
        }
    
@route('/calification', method='POST')
def create_calification():
    response.content_type = 'application/json'
    calification = CalificationSchema()
    body = json.loads(request.body.read())
    res = calification.load(body)
    if res.errors:
        return {
            "success" : False,
            "errors" : res.errors
        }
    else:
        new_cal = CalificationModel(
            course = body["course"],
            credits = body["credits"],
            calification = body["calification"],
            notes = body["notes"],
            user_id = body["user_id"],        
            )
        try:
            new_cal.save()
            return {
                "success" : True,
                "data" : res.data
            }
        except  Exception as e:
            return {
            "success" : False,
            "errors" : "El registro existe en BD"
        }

@route('/calification/<id>', method='GET')
def create_calification(id):
    rows = CalificationModel.select().where(CalificationModel.user_id==id)
    res = []
    for r in rows:
        data = {}
        data["course"] = r.course
        data["credits"] = r.credits
        data["calification"] = r.calification
        data["notes"] = r.notes
        res.append(data)
    response.content_type = 'application/json'
    return json.dumps(res)

run(host='localhost', port=8000, debug=True)
