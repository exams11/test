from marshmallow import Schema, fields, validate, post_load



class UserSchema(Schema): 
   dni = fields.String(required=True, validate=validate.Length(max=8))
   nombre = fields.String()
   apellido = fields.String()
   password = fields.String(required=True)
   email = fields.Email(required=True)
   


class CalificationSchema(Schema):
   calification_id = fields.String()
   course = fields.String(required=True)
   credits = fields.String(required=True)
   calification = fields.String(required=True, validate=validate.Length(max=2))
   notes = fields.String()
   user_id = fields.Integer(required=True)
   
